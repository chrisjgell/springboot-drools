package com.example.springbootdrools.entities;

import lombok.Data;

@Data
public class TaxRate {
    private Double taxRate;
}