package com.example.springbootdrools.entities;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Trade {
    private String countryOfQuotation;
    private String executingBroker;
}