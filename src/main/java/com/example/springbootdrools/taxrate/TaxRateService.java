package com.example.springbootdrools.taxrate;

import com.example.springbootdrools.entities.TaxRate;
import com.example.springbootdrools.entities.Trade;

import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.StatelessKieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TaxRateService {

    private final KieContainer kieContainer;

    @Autowired
    public TaxRateService(KieContainer kieContainer) {
        this.kieContainer = kieContainer;
    }

    public TaxRate getTaxRate(Trade trade) {
        log.debug("running rules against: " + trade);
        StatelessKieSession kieSession = kieContainer.newStatelessKieSession("trade-tax-rate-session");
        TaxRate taxRate = new TaxRate();
        kieSession.setGlobal("taxRate", taxRate);
        kieSession.execute(trade);
        return taxRate;
    }
}