package com.example.springbootdrools.taxrate;

import com.example.springbootdrools.entities.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class TaxRateController {
    
    @Autowired
    private TaxRateService TaxRateService;

    @PostMapping("/taxrate")
    public ResponseEntity<TaxRate> taxRate(@RequestBody Trade trade) {
        TaxRate taxRate = TaxRateService.getTaxRate(trade);
        return ResponseEntity.ok(taxRate);
    }
}